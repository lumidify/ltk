/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <SDL2/SDL.h>
#include "widget.h"
#include "uthash.h"

#ifndef _LTK_WINDOW_H_
#define _LTK_WINDOW_H_

typedef struct
{
    Uint32 key;
    SDL_Window *sdl_window;
    SDL_Renderer *renderer;
    LtkWidgetBase *root_widget;
    UT_hash_handle hh;
} LtkWindow;

LtkWindow *ltk_window_hash;

void ltk_show_window(LtkWindow *window);

void ltk_hide_window(LtkWindow *window);

void ltk_window_init(void);

void ltk_draw_window(LtkWindow *window);

void ltk_set_window_title(LtkWindow *window, const char *title);

LtkWindow *ltk_create_window(const char *title, int x, int y, int w, int h, Uint32 flags);
void ltk_destroy_window(LtkWindow *window);
void ltk_resize_window(Uint32 id, int w, int h);
void ltk_window_set_root_widget(LtkWindow *window, void *root_widget);

#endif
