/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "grid.h"

/* TODO: remove_widget function that also adjusts static width */

void ltk_set_row_weight(LtkGrid *grid, int row, int weight)
{
    grid->row_weights[row] = weight;
    ltk_recalculate_grid(grid);
}

void ltk_set_column_weight(LtkGrid *grid, int column, int weight)
{
    grid->column_weights[column] = weight;
    ltk_recalculate_grid(grid);
}

void ltk_draw_grid(LtkGrid *grid)
{
    int i;
    for (i = 0; i < grid->rows * grid->columns; i++)
    {
        if (!grid->widget_grid[i])
        {
            continue;
        }
        LtkWidgetBase *ptr = (LtkWidgetBase *)(grid->widget_grid[i]);
        ptr->draw_function(ptr);
    }
}

LtkGrid *ltk_create_grid(SDL_Renderer *renderer, int rows, int columns)
{
    LtkGrid *grid = malloc(sizeof(LtkGrid));

    grid->widget.renderer = renderer;
    grid->widget.update_function = &ltk_recalculate_grid;
    grid->widget.draw_function = &ltk_draw_grid;
    grid->widget.destroy_function = &ltk_destroy_grid;
    grid->widget.rect.x = 0;
    grid->widget.rect.y = 0;
    grid->widget.rect.w = 0;
    grid->widget.rect.h = 0;

    grid->rows = rows;
    grid->columns = columns;
    grid->widget_grid = malloc(rows * columns * sizeof(LtkWidgetBase *));
    grid->row_heights = malloc(rows * sizeof(int));
    grid->column_widths = malloc(rows * sizeof(int));
    grid->row_weights = malloc(rows * sizeof(int));
    grid->column_weights = malloc(columns * sizeof(int));
    /* Positions have one extra for the end */
    grid->row_pos = malloc((rows + 1) * sizeof(int));
    grid->column_pos = malloc((columns + 1) * sizeof(int));
    int i;
    for (i = 0; i < rows; i++)
    {
        grid->row_heights[i] = 0;
        grid->row_weights[i] = 0;
        grid->row_pos[i] = 0;
    }
    grid->row_pos[rows] = 0;
    for (i = 0; i < columns; i++)
    {
        grid->column_widths[i] = 0;
        grid->column_weights[i] = 0;
        grid->column_pos[i] = 0;
    }
    grid->column_pos[columns] = 0;
    for (i = 0; i < rows * columns; i++)
    {
        grid->widget_grid[i] = NULL;
    }

    ltk_recalculate_grid(grid);
    return grid;
}

void ltk_destroy_grid(void *widget)
{
    LtkGrid *grid = (LtkGrid *)widget;
    int i;
    for (i = 0; i < grid->rows * grid->columns; i++)
    {
        if (grid->widget_grid[i])
        {
            grid->widget_grid[i]->destroy_function(grid->widget_grid[i]);
        }
    }
    free(grid->widget_grid);
    free(grid->row_heights);
    free(grid->column_widths);
    free(grid->row_weights);
    free(grid->column_weights);
    free(grid->row_pos);
    free(grid->column_pos);
    free(grid);
}

void ltk_recalculate_grid(void *widget)
{
    LtkGrid *grid = (LtkGrid *)widget;
    unsigned int height_static = 0, width_static = 0;
    unsigned int total_row_weight = 0, total_column_weight = 0;
    float height_unit = 0, width_unit = 0;
    unsigned int currentx = 0, currenty = 0;
    int i, j;
    for (i = 0; i < grid->rows; i++)
    {
        total_row_weight += grid->row_weights[i];
        if (grid->row_weights[i] == 0)
        {
            height_static += grid->row_heights[i];
        }
    }
    for (i = 0; i < grid->columns; i++)
    {
        total_column_weight += grid->column_weights[i];
        if (grid->column_weights[i] == 0)
        {
            width_static += grid->column_widths[i];
        }
    }
    if (total_row_weight > 0)
    {
        height_unit = (float)(grid->widget.rect.h - height_static) / (float)total_row_weight;
    }
    if (total_column_weight > 0)
    {
        width_unit = (float)(grid->widget.rect.w - width_static) / (float)total_column_weight;
    }
    for (i = 0; i < grid->rows; i++)
    {
        grid->row_pos[i] = currenty;
        if (grid->row_weights[i] > 0)
        {
            grid->row_heights[i] = grid->row_weights[i] * height_unit;
        }
        currenty += grid->row_heights[i];
    }
    grid->row_pos[grid->rows] = currenty;
    for (i = 0; i < grid->columns; i++)
    {
        grid->column_pos[i] = currentx;
        if (grid->column_weights[i] > 0)
        {
            grid->column_widths[i] = grid->column_weights[i] * width_unit;
        }
        currentx += grid->column_widths[i];
    }
    grid->column_pos[grid->columns] = currentx;
    int orig_width, orig_height;
    int end_column, end_row;
    for (i = 0; i < grid->rows; i++)
    {
        for (j = 0; j < grid->columns; j++)
        {
            if (!grid->widget_grid[i * grid->columns + j])
            {
                continue;
            }
            LtkWidgetBase *ptr = (LtkWidgetBase *)(grid->widget_grid[i * grid->columns + j]);
            orig_width = ptr->rect.w;
            orig_height = ptr->rect.h;
            end_row = i + ptr->row_span;
            end_column = j + ptr->column_span;
            if (ptr->sticky[1] == 1 && ptr->sticky[3] == 1)
            {
                ptr->rect.w = grid->column_pos[end_column] - grid->column_pos[j];
            }
            if (ptr->sticky[0] == 1 && ptr->sticky[2] == 1)
            {
                ptr->rect.h = grid->row_pos[end_row] - grid->row_pos[i];
            }
            if (orig_width != ptr->rect.w || orig_height != ptr->rect.h)
            {
                ptr->update_function(ptr);
            }

            if (ptr->sticky[1] == 1)
            {
                ptr->rect.x = grid->column_pos[end_column] - ptr->rect.w;
            }
            else if (ptr->sticky[3] == 1)
            {
                ptr->rect.x = grid->column_pos[j];
            }
            else
            {
                ptr->rect.x = grid->column_pos[j] + ((grid->column_pos[end_column] - grid->column_pos[j]) / 2 - ptr->rect.w / 2);
            }

            if (ptr->sticky[2] == 1)
            {
                ptr->rect.y = grid->row_pos[end_row] - ptr->rect.h;
            }
            else if (ptr->sticky[0] == 1)
            {
                ptr->rect.y = grid->row_pos[i];
            }
            else
            {
                ptr->rect.y = grid->row_pos[i] + ((grid->row_pos[end_row] - grid->row_pos[i]) / 2 - ptr->rect.h / 2);
            }
        }
    }
}

void ltk_grid_widget(void *ptr, LtkGrid *grid, int row, int column, int row_span, int column_span, int sticky[4])
{
    LtkWidgetBase *widget = (LtkWidgetBase *)ptr;
    memcpy(widget->sticky, sticky, 4 * sizeof(int));
    widget->row = row;
    widget->column = column;
    widget->row_span = row_span;
    widget->column_span = column_span;
    if (grid->column_weights[column] == 0 && widget->rect.w > grid->column_widths[column]) {
        grid->column_widths[column] = widget->rect.w;
    }
    if (grid->row_weights[row] == 0 && widget->rect.h > grid->row_heights[row]) {
        grid->row_heights[row] = widget->rect.h;
    }
    grid->widget_grid[widget->row * grid->columns + widget->column] = widget;
    ltk_recalculate_grid(grid);
}
