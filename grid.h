/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "widget.h"

#ifndef _LTK_GRID_H_
#define _LTK_GRID_H_

typedef struct
{
    LtkWidgetBase widget;
    unsigned int rows;
    unsigned int columns;
    LtkWidgetBase **widget_grid;
    unsigned int *row_heights;
    unsigned int *column_widths;
    unsigned int *row_weights;
    unsigned int *column_weights;
    unsigned int *row_pos;
    unsigned int *column_pos;
} LtkGrid;

void ltk_set_row_weight(LtkGrid *grid, int row, int weight);
void ltk_set_column_weight(LtkGrid *grid, int column, int weight);
void ltk_draw_grid(LtkGrid *grid);
LtkGrid *ltk_create_grid(SDL_Renderer *renderer, int rows, int columns);
void ltk_destroy_grid(void *widget);
void ltk_recalculate_grid(void *widget);
void ltk_grid_widget(void *ptr, LtkGrid *grid, int row, int column, int rowspan, int columnspan, int sticky[4]);

#endif
