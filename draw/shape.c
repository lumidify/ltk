/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "shape.h"
#include "Gradient.h"

int main(int argc, char *argv[])
{
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
                exit(1);
        }

        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
                printf("Warning: Linear texture filtering not enabled!\n");
        }

        SDL_Window *window = SDL_CreateWindow("Circle Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 500, 500, SDL_WINDOW_SHOWN);
        if (window == NULL) {
                printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
        }

        SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

        if (renderer == NULL) {
                printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
                exit(1);
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        /*
        point *p1 = malloc(sizeof(point));
        point *p2 = malloc(sizeof(point));
        point *p3 = malloc(sizeof(point));
        point *p4 = malloc(sizeof(point));
        p1->x = 0;
        p1->y = 0;
        p2->x = 500;
        p2->y = 50;
        p3->x = 450;
        p3->y = 500;
        p4->x = 0;
        p4->y = 0;
        point *points[4] = {p1, p2, p3, p4};
        LTK_DrawFilledPolygon(renderer, points, 4, 255, 255, 255, 255);

        LTK_DrawCircleAntialiased(renderer, 250, 250, 50, 255, 255, 255, 255);
        */

        int points[] = {0, 0, 500, 100, 200, 400, 0, 0};
        LTK_DrawPolygonAliased(renderer, points, 4, 255, 255, 255, 255);
        LTK_DrawLineAliased(renderer, 100, 0, 100, 500, 255, 255, 255, 255);
        LTK_DrawLineAliased(renderer, 0, 100, 500, 100, 255, 255, 255, 255);
        LTK_DrawVerticalGradient(renderer, 100, 10, 100, 100, 255, 255, 255, 255, 0, 0, 0, 255);

        SDL_RenderPresent(renderer);
        
        SDL_Delay(10000);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        
        return 0;
}

void LTK_DrawCircleAntialiased(SDL_Renderer *renderer, int x0, int y0, int radius, int r, int g, int b, int a)
{
}
