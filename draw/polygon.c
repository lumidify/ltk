/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "shape.h"
/*
void AntiEdge(SDL_Renderer *renderer, int x0, int y0, int xn, int yn)
{
int dx,dy,q,p,dis,side,flag;
int i,j,x,y,xm,ym;
unsigned char cr,cg,cb,fr,fg,fb,C1;
unsigned int *pat_p,*dis_p,r;
side=0;
if (yn<y0)
{
        int temp = x0;
        x0 = xn;
        xn = temp;
        temp = y0;
        y0 = yn;
        yn = temp;
        side ^= 1;
}
if (xn<x0)
{ x0=-x0; xn=-xn; flag=10; side^=1; }
dy=yn-y0;
dx=xn-x0;
if (dx<dy)
{
        int temp = x0;
        x0 = y0;
        y0 = temp;
        temp = xn;
        xn = yn;
        yn = temp;
        temp = dy;
        dy = dx;
        dx = temp;
        flag++;
        side^=1;
}
i=(dy*2046+dx)/(dx<<1);
q=SlopeTable[i].Q;
p=SlopeTable[i].P;
pat_p=SlopeTable[i].XP;
dis_p=SlopeTable[i].Dis;
x=x0; y=y0;
r=*patp++;
j=32;
for (i=0; i<q; i++) {
if (r&0x01) y++;
x++;
r=r>>1;
if (--j==0)
{ r=*patp++; j=32; }
dis=*dis_p++;
C1=16-dis;
xm=x; ym=y;
do {
if (side==0) {
fr=(C1*cr+dis*fr)/16;
fg=(C1*cg+dis*fg)/16;
fb=(C1*cb+dis*fb)/16;
}
else {
fr=(C1*fr+dis*cr)/16;
fg=(C1*fg+dis*cg)/16;
fb=(C1*fb+dis*cb)/16;
}
WritePixel(xm,ym,flag);
xm+=q; ym+=p;
} while (xm<=xn);
}
}
*/
static void _draw_polygon(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a, void (*line_function)(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a))
{
        for (int i = 0; i < point_number - 1; i++) {
                line_function(renderer, points[i * 2], points[i * 2 + 1], points[(i + 1) * 2], points[(i + 1) * 2 + 1], r, g, b, a);
        }
}

void LTK_DrawPolygonAntialiased(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a)
{
        _draw_polygon(renderer, points, point_number, r, g, b, a, LTK_DrawLineAntialiased);
}

void LTK_DrawPolygonAliased(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a)
{
        _draw_polygon(renderer, points, point_number, r, g, b, a, LTK_DrawLineAliased);
}

/*
 * Public-domain code by Darel Rex Finley, 2007
 */
void LTK_DrawFilledPolygon(SDL_Renderer *renderer, point *points[], int point_number, int r, int g, int b, int a)
{
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
        for (int bob = 0; bob < point_number - 1; bob++) {
                LTK_DrawLineAntialiased(renderer, points[bob]->x, points[bob]->y, points[bob + 1]->x, points[bob + 1]->y, r, g, b, a);
        }
        return;
        
        int  nodes, pixelX, pixelY, i, j, swap;
        double nodeX[point_number];

        int r0 = 255;
        int g0 = 255;
        int b0 = 255;
        int a0 = 255;
        int r1 = 0;
        int g1 = 0;
        int b1 = 0;
        int a1 = 255;
        
        int rdiff = r1 - r0;
        int gdiff = g1 - g0;
        int bdiff = b1 - b0;
        int adiff = a1 - a0;
        int height = 500;
        
        for (pixelY = 0; pixelY < 500; pixelY++) {
                r = r1 + rdiff * ((double)pixelY / (double)height);
                g = g1 + gdiff * ((double)pixelY / (double)height);
                b = b1 + bdiff * ((double)pixelY / (double)height);
                a = a1 + adiff * ((double)pixelY / (double)height);
                SDL_SetRenderDrawColor(renderer, r, g, b, a);
                
                //  Build a list of nodes.
                nodes = 0; j = point_number - 1;
                for (i = 0; i < point_number; i++) {
                        if (points[i]->y < (double)pixelY && points[j]->y >= (double)pixelY
                            || points[j]->y < (double)pixelY && points[i]->y >= (double)pixelY) {
                                nodeX[nodes++] = (double)(points[i]->x + (pixelY - points[i]->y) / (double)(points[j]->y - points[i]->y)
                                                      * (points[j]->x - points[i]->x));
                        }
                        j = i;
                }

                i = 0;
                while (i < nodes - 1) {
                        if (nodeX[i]>nodeX[i+1]) {
                                swap = nodeX[i];
                                nodeX[i] = nodeX[i+1];
                                nodeX[i+1] = swap;
                                if (i) i--;
                        } else {
                                i++;
                        }
                }

                for (i = 0; i < nodes; i += 2) {
                        if (nodeX[i  ] >= 500) break;
                        if (nodeX[i+1] > 0 ) {
                                if (nodeX[i] < 0 ) nodeX[i] = 0 ;
                                if (nodeX[i+1] > 500) nodeX[i+1] = 500;
                                for (pixelX = nodeX[i]; pixelX < nodeX[i+1]; pixelX++) SDL_RenderDrawPoint(renderer, pixelX, pixelY);
                        }
                }
        }
}
