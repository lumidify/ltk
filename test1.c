#include "ltk.h"

int main(int argc, char *argv[])
{
    LtkTheme *theme = ltk_load_theme("themes/default.json");
    LtkWindow *window1 = ltk_create_window("Cool Window!", 0, 0, 500, 500, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    LtkGrid *grid1 = ltk_create_grid(window1->renderer, 2, 2);
    ltk_set_row_weight(grid1, 0, 1);
    ltk_set_row_weight(grid1, 1, 1);
    ltk_set_column_weight(grid1, 0, 1);
    ltk_set_column_weight(grid1, 1, 1);
    ltk_window_set_root_widget(window1, grid1);
    ltk_show_window(window1);
    LtkButton *button1 = ltk_create_button(theme, window1->renderer, "I'm a button!", NULL);
    int sticky1[4] = {0, 1, 0, 1};
    ltk_grid_widget(button1, grid1, 0, 0, 1, 2, sticky1);
    /*LtkButton *button2 = ltk_create_button(theme, window1->renderer, "I'm a button!", NULL);
    ltk_grid_widget(button2, grid1, 0, 1, 1, 1, sticky1);*/
    LtkButton *button3 = ltk_create_button(theme, window1->renderer, "I'm a button!", NULL);
    ltk_grid_widget(button3, grid1, 1, 0, 1, 1, sticky1);
    LtkButton *button4 = ltk_create_button(theme, window1->renderer, "I'm a button!", NULL);
    ltk_grid_widget(button4, grid1, 1, 1, 1, 1, sticky1);
    ltk_mainloop();
    ltk_destroy_theme(theme);
}
