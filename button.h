/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <SDL2/SDL.h>
#include "widget.h"
#include "theme.h"

#ifndef _LTK_BUTTON_H_
#define _LTK_BUTTON_H_

typedef struct
{
    LtkWidgetBase widget;
    void (*callback)(void);
    char *text;
    int pressed;
    int hover;
    int active;
    SDL_Texture *normal_image;
    SDL_Texture *pressed_image;
    SDL_Texture *hover_image;
    SDL_Texture *active_image;
    LtkButtonTheme *theme;
} LtkButton;

LtkButtonTheme *ltk_parse_button_theme(cJSON *button_json);
static void ltk_update_button_image(LtkButtonTheme *theme, SDL_Renderer *renderer, SDL_Texture *texture, SDL_Rect button_rect);
void ltk_update_button_images(void *widget);
LtkButton *ltk_create_button(LtkTheme *theme, SDL_Renderer *renderer, const char *text, void (*callback)(void));
void ltk_destroy_button(void *widget);

#endif
