/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>

#ifndef _LTK_SHAPE_H_
#define _LTK_SHAPE_H_

typedef struct {
        int x;
        int y;
} point;

void LTK_DrawCircleAliased(SDL_Renderer *renderer, int x0, int y0, int radius, int r, int g, int b, int a);

void LTK_DrawCircleAntialiased(SDL_Renderer *renderer, int x0, int y0, int radius, int r, int g, int b, int a);

void LTK_DrawLineAliased(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a);

void LTK_DrawLineAntialiased(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a);

void LTK_DrawFilledPolygon(SDL_Renderer *renderer, point *points[], int point_number, int r, int g, int b, int a);

void LTK_DrawPolygonAntialiased(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a);

void LTK_DrawPolygonAliased(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a);

static void _draw_polygon(SDL_Renderer *renderer, int points[], int point_number, int r, int g, int b, int a, void (*line_function)(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a));

static int ipart(double x);

static double fpart(double x);

static double rfpart(double x);

#endif
