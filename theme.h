/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "common.h"
#include "cJSON.h"

#ifndef _LTK_THEME_H_
#define _LTK_THEME_H_

typedef struct
{
    int border_width;
    int font_size;
    LtkRGBA border_color;
    LtkRGBA fill_color;
    int padding_left;
    int padding_right;
    int padding_top;
    int padding_bottom;

    int border_width_hover;
    int font_size_hover;
    LtkRGBA border_color_hover;
    LtkRGBA fill_color_hover;

    int border_width_pressed;
    int font_size_pressed;
    LtkRGBA border_color_pressed;
    LtkRGBA fill_color_pressed;

    int border_width_active;
    int font_size_active;
    LtkRGBA border_color_active;
    LtkRGBA fill_color_active;

} LtkButtonTheme;

typedef struct
{
    LtkButtonTheme *button;
} LtkTheme;

LtkTheme *ltk_load_theme(const char *path);
void ltk_destroy_theme(LtkTheme *theme);

#endif
