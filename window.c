/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "window.h"

void ltk_show_window(LtkWindow *window)
{
    SDL_ShowWindow(window->sdl_window);
}

void ltk_hide_window(LtkWindow *window)
{
    SDL_HideWindow(window->sdl_window);
}

void ltk_set_window_title(LtkWindow *window, const char *title)
{
    SDL_SetWindowTitle(window->sdl_window, title);
}

void ltk_window_init(void)
{
    ltk_window_hash = NULL;
}

void ltk_draw_window(LtkWindow *window)
{
    if (!window)
    {
        return;
    }
    if (!window->root_widget)
    {
        return;
    }
    SDL_SetRenderDrawColor(window->renderer, 0, 0, 0, 255);
    SDL_RenderClear(window->renderer);
    window->root_widget->draw_function(window->root_widget);
    SDL_RenderPresent(window->renderer);
}

LtkWindow *ltk_create_window(const char *title, int x, int y, int w, int h, Uint32 flags)
{
    flags |= SDL_WINDOW_HIDDEN;
    LtkWindow *window = malloc(sizeof(LtkWindow));
    SDL_Window *sdl_window = SDL_CreateWindow(title, x, y, w, h, flags);
    if (!(sdl_window))
    {
        printf("ERROR: Window could not be created! SDL Error: %s\n", SDL_GetError());
        exit(1);
    }
    window->sdl_window = sdl_window;
    window->key = SDL_GetWindowID(sdl_window);
    window->renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
    if (!(window->renderer))
    {
        printf("ERROR: Renderer could not be created! SDL Error: %s\n", SDL_GetError());
        exit(1);
    }

    window->root_widget = NULL;

    HASH_ADD_INT(ltk_window_hash, key, window);

    return window;
}

void ltk_destroy_window(LtkWindow *window)
{
    if (window->root_widget)
    {
        window->root_widget->destroy_function(window->root_widget);
    }
    SDL_DestroyRenderer(window->renderer);
    SDL_DestroyWindow(window->sdl_window);
    HASH_DEL(ltk_window_hash, window);
    free(window);
}

void ltk_resize_window(Uint32 id, int w, int h)
{
    LtkWindow *window;
    HASH_FIND_INT(ltk_window_hash, &id, window);
    if (!window)
    {
        return;
    }
    if (!window->root_widget)
    {
        return;
    }
    window->root_widget->rect.w = w;
    window->root_widget->rect.h = h;
    window->root_widget->update_function(window->root_widget);
}

void ltk_window_set_root_widget(LtkWindow *window, void *root_widget)
{
    window->root_widget = (LtkWidgetBase *)root_widget;
}
