/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <SDL2/SDL.h>
#include <stdio.h>
#include "Gradient.h"

/*
 * renderer: the SDL_Renderer to draw the gradient on
 * x, y: the x and y coordinates of the top left corner of the gradient
 * width, height: the width and height of the gradient
 * r1, g1, b1, a1: the starting red, green, blue and alpha values of the gradient
 * r2, g2, b2, a2: the ending red, gree, blue and alpha values of the gradient
 */
int LTK_DrawVerticalGradient(SDL_Renderer *renderer, int x, int y, int width, int height, int r1, int g1, int b1, int a1, int r2, int g2, int b2, int a2)
{
        
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        int rdiff = r2 - r1;
        int gdiff = g2 - g1;
        int bdiff = b2 - b1;
        int adiff = a2 - a1;

        int r;
        int g;
        int b;
        int a;
        
        for (int i = 0; i < height; i++) {
                r = r1 + rdiff * ((double)i / (double)height);
                g = g1 + gdiff * ((double)i / (double)height);
                b = b1 + bdiff * ((double)i / (double)height);
                a = a1 + adiff * ((double)i / (double)height);
                SDL_SetRenderDrawColor(renderer, r, g, b, a);
                SDL_RenderDrawLine(renderer, x, y + i, x + width, y + i);
        }
}

/*
 * renderer: the SDL_Renderer to draw the gradient on
 * x, y: the x and y coordinates of the top left corner of the gradient
 * width, height: the width and height of the gradient
 * r1, g1, b1, a1: the starting red, green, blue and alpha values of the gradient
 * r2, g2, b2, a2: the ending red, gree, blue and alpha values of the gradient
 */
int LTK_DrawHorizontalGradient(SDL_Renderer *renderer, int x, int y, int width, int height, int r1, int g1, int b1, int a1, int r2, int g2, int b2, int a2)
{
        
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        int rdiff = r2 - r1;
        int gdiff = g2 - g1;
        int bdiff = b2 - b1;
        int adiff = a2 - a1;

        int r;
        int g;
        int b;
        int a;
        
        for (int i = 0; i < width; i++) {
                r = r1 + rdiff * ((double)i / (double)width);
                g = g1 + gdiff * ((double)i / (double)width);
                b = b1 + bdiff * ((double)i / (double)width);
                a = a1 + adiff * ((double)i / (double)width);
                SDL_SetRenderDrawColor(renderer, r, g, b, a);
                SDL_RenderDrawLine(renderer, x + i, y, x + i, y + height);
        }
}

int main(int argc, char *argv[])
{
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
                exit(1);
        }
        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
                printf("Warning: Linear texture filtering not enabled!\n");
        }
        SDL_Window *window = SDL_CreateWindow("Gradient Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 500, 500, SDL_WINDOW_SHOWN);
        if (window == NULL) {
                printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
                exit(1);
        }
        SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        if (renderer == NULL) {
                printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
                exit(1);
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);

        LTK_DrawVerticalGradient(renderer, 0, 0, 100, 50, 0, 0, 0, 255, 255, 255, 255, 255);
        
        SDL_RenderPresent(renderer);
        SDL_Delay(20000);

        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);

        return 0;
}
