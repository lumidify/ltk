LIBS = -lSDL2 -lm -ldl

all: test1.c
	gcc -std=c89 -g -w -Wall -Werror -Wextra -pedantic $(LIBS) event.c ltk.c cJSON.c common.c theme.c button.c grid.c window.c test1.c -o main
