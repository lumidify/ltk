/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "shape.h"

/*
 * Draw a circle using the midpoint circle algorithm
 * https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
 */
void LTK_DrawCircleAliased(SDL_Renderer *renderer, int x0, int y0, int radius, int r, int g, int b, int a)
{
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(renderer, r, g, b, a);

        SDL_RenderDrawPoint(renderer, x0, y0 + radius);
        SDL_RenderDrawPoint(renderer, x0, y0 - radius);
        SDL_RenderDrawPoint(renderer, x0 + radius, y0);
        SDL_RenderDrawPoint(renderer, x0 - radius, y0);
        int x = 0, y = radius;
        int dp = 1 - radius;
        do {
                if (dp < 0) {
                        dp = dp + 2 * (++x) + 3;
                } else {
                        dp = dp + 2 * (++x) - 2 * (--y) + 5;
                }

                SDL_RenderDrawPoint(renderer, x0 + x, y0 + y);
                SDL_RenderDrawPoint(renderer, x0 - x, y0 + y);
                SDL_RenderDrawPoint(renderer, x0 + x, y0 - y);
                SDL_RenderDrawPoint(renderer, x0 - x, y0 - y);
                SDL_RenderDrawPoint(renderer, x0 + y, y0 + x);
                SDL_RenderDrawPoint(renderer, x0 - y, y0 + x);
                SDL_RenderDrawPoint(renderer, x0 + y, y0 - x);
                SDL_RenderDrawPoint(renderer, x0 - y, y0 - x);

        } while (x < y);
}
void LTK_DrawRoundedRect(SDL_Renderer *renderer, int x, int y, int width, int height, int radius, int r, int g, int b, int a)
{}
/*
void LTK_DrawCircleAliased(SDL_Renderer *renderer, int x0, int y0, int radius, int r, int g, int b, int a)
{
        /*
         * This is a stub. The algorithm still needs to be implemented.
         */
}
*/
