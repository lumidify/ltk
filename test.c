#include <stdio.h>
#include <SDL2/SDL.h>

int main(void)
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
    SDL_Window *window = SDL_CreateWindow("Bob", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 500, 500, SDL_WINDOW_SHOWN);
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    int done = 0;
    SDL_StartTextInput();
    SDL_Rect bob;
    bob.x = 0;
    bob.y = 0;
    bob.w = 500;
    bob.h = 500;
    SDL_SetTextInputRect(&bob);
    while (!done)
    {
        SDL_Event event;
        if (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                /* Quit */
                done = 1;
                break;
            case SDL_TEXTINPUT:
                /* Add new text onto the end of our text */
                printf("INPUT: %s\n", event.text.text);
                break;
            case SDL_TEXTEDITING:
                /*
                  Update the composition text.
                  Update the cursor position.
                  Update the selection length (if any).
                */
                printf("EDIT: %s, %d, %d\n", event.edit.text, event.edit.start, event.edit.length);
                break;
            }
        }
    }
    SDL_StopTextInput();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    return 0;
}
