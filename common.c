/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "common.h"

LtkRGBA ltk_split_hex(const char *hex)
{
    if (strlen(hex) != 8)
    {
        printf("Error splitting \"%s\" into r, g, b and a values.\n", hex);
        LtkRGBA color = {.r = 0, .g = 0, .b = 0, .a = 0};
        return color;
    }

    char r[3];
    char g[3];
    char b[3];
    char a[3];
    strncpy(r, hex, 2);
    strncpy(g, hex + 2, 2);
    strncpy(b, hex + 4, 2);
    strncpy(a, hex + 6, 2);
    r[2] = '\0';
    g[2] = '\0';
    b[2] = '\0';
    a[2] = '\0';
    int ri = strtol(r, NULL, 16);
    int gi = strtol(g, NULL, 16);
    int bi = strtol(b, NULL, 16);
    int ai = strtol(a, NULL, 16);
    LtkRGBA color = {.r = ri, .g = gi, .b = bi, .a = ai};
    return color;
}

char *ltk_read_file(const char *path)
{
    FILE *f;
    long len;
    char *file_contents;
    f = fopen(path, "rb");
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    fseek(f, 0, SEEK_SET);
    file_contents = malloc(len + 1);
    fread(file_contents, 1, len, f);
    file_contents[len] = '\0';
    fclose(f);

    return file_contents;
}
