/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "button.h"

LtkButtonTheme *ltk_parse_button_theme(cJSON *button_json)
{
    LtkButtonTheme *button_theme = malloc(sizeof(LtkButtonTheme));
    cJSON *normal_json = cJSON_GetObjectItem(button_json, "normal");
    if (!normal_json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
    }
    cJSON *border_width = cJSON_GetObjectItem(normal_json, "border-width");
    cJSON *font_size = cJSON_GetObjectItem(normal_json, "font-size");
    cJSON *border_color = cJSON_GetObjectItem(normal_json, "border-color");
    cJSON *fill_color = cJSON_GetObjectItem(normal_json, "fill-color");
    cJSON *padding_left = cJSON_GetObjectItem(normal_json, "padding-left");
    cJSON *padding_right = cJSON_GetObjectItem(normal_json, "padding-right");
    cJSON *padding_top = cJSON_GetObjectItem(normal_json, "padding-top");
    cJSON *padding_bottom = cJSON_GetObjectItem(normal_json, "padding-bottom");

    button_theme->border_width = border_width != NULL ? border_width->valueint : 0;
    button_theme->font_size = font_size != NULL ? font_size->valueint : 20;
    button_theme->border_color = ltk_split_hex(border_color->valuestring);
    button_theme->fill_color = ltk_split_hex(fill_color->valuestring);
    button_theme->padding_left = padding_left != NULL ? padding_left->valueint : 0;
    button_theme->padding_right = padding_right != NULL ? padding_right->valueint : 0;
    button_theme->padding_top = padding_top != NULL ? padding_top->valueint : 0;
    button_theme->padding_bottom = padding_bottom != NULL ? padding_bottom->valueint : 0;

    cJSON *hover_json = cJSON_GetObjectItem(button_json, "hover");
    if (!hover_json)
	{
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
    }
    cJSON *border_width_hover = cJSON_GetObjectItem(hover_json, "border-width");
    cJSON *font_size_hover = cJSON_GetObjectItem(hover_json, "font-size");
    cJSON *border_color_hover = cJSON_GetObjectItem(hover_json, "border-color");
    cJSON *fill_color_hover = cJSON_GetObjectItem(hover_json, "fill-color");

    button_theme->border_width_hover = border_width_hover != NULL ? border_width_hover->valueint : button_theme->border_width;
    button_theme->font_size_hover = font_size_hover != NULL ? font_size_hover->valueint : button_theme->font_size;
    button_theme->border_color_hover = border_color_hover != NULL ? ltk_split_hex(border_color_hover->valuestring) : button_theme->border_color;
    button_theme->fill_color_hover = fill_color_hover != NULL ? ltk_split_hex(fill_color_hover->valuestring) : button_theme->fill_color;

    cJSON *pressed_json = cJSON_GetObjectItem(button_json, "pressed");
    if (!pressed_json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
    }
    cJSON *border_width_pressed = cJSON_GetObjectItem(pressed_json, "border-width");
    cJSON *font_size_pressed = cJSON_GetObjectItem(pressed_json, "font-size");
    cJSON *border_color_pressed = cJSON_GetObjectItem(pressed_json, "border-color");
    cJSON *fill_color_pressed = cJSON_GetObjectItem(pressed_json, "fill-color");

    button_theme->border_width_pressed = border_width_pressed != NULL ? border_width_pressed->valueint : button_theme->border_width;
    button_theme->font_size_pressed = font_size_hover != NULL ? font_size_pressed->valueint : button_theme->font_size;
    button_theme->border_color_pressed = border_color_pressed != NULL ? ltk_split_hex(border_color_pressed->valuestring) : button_theme->border_color;
    button_theme->fill_color_pressed = fill_color_pressed != NULL ? ltk_split_hex(fill_color_pressed->valuestring) : button_theme->fill_color;

    cJSON *active_json = cJSON_GetObjectItem(button_json, "active");
    if (!active_json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
    }
    cJSON *border_width_active = cJSON_GetObjectItem(active_json, "border-width");
    cJSON *font_size_active = cJSON_GetObjectItem(active_json, "font-size");
    cJSON *border_color_active = cJSON_GetObjectItem(active_json, "border-color");
    cJSON *fill_color_active = cJSON_GetObjectItem(active_json, "fill-color");

    button_theme->border_width_active = border_width_active != NULL ? border_width_active->valueint : button_theme->border_width;
    button_theme->font_size_active = font_size_active != NULL ? font_size_active->valueint : button_theme->font_size;
    button_theme->border_color_active = border_color_active != NULL ? ltk_split_hex(border_color_active->valuestring) : button_theme->border_color;
    button_theme->fill_color_active = fill_color_active != NULL ? ltk_split_hex(fill_color_active->valuestring) : button_theme->fill_color;

    return button_theme;
}

static void ltk_update_button_image(LtkButtonTheme *theme, SDL_Renderer *renderer, SDL_Texture *texture, SDL_Rect button_rect)
{
    SDL_Rect rect;
    SDL_SetRenderTarget(renderer, texture);
    SDL_SetRenderDrawColor(renderer, theme->border_color.r, theme->border_color.g, theme->border_color.b, theme->border_color.a);
    int i;
    for (i = 0; i < theme->border_width; i++)
    {
        rect.x = i;
        rect.y = i;
        rect.w = button_rect.w - 2 * i;
        rect.h = button_rect.h - 2 * i;
        SDL_RenderDrawRect(renderer, &rect);
    }

    rect.x = theme->border_width;
    rect.y = theme->border_width;
    rect.w = button_rect.w - 2 * theme->border_width;
    rect.h = button_rect.h - 2 * theme->border_width;
    SDL_SetRenderDrawColor(renderer, theme->fill_color.r, theme->fill_color.g, theme->fill_color.b, theme->fill_color.a);
    SDL_RenderFillRect(renderer, &rect);
}

void ltk_update_button_images(void *widget)
{
    LtkButton *button = (LtkButton *)widget;
    if (button->normal_image) SDL_DestroyTexture(button->normal_image);
    if (button->pressed_image) SDL_DestroyTexture(button->normal_image);
    if (button->hover_image) SDL_DestroyTexture(button->hover_image);
    if (button->active_image) SDL_DestroyTexture(button->active_image);

    button->normal_image = SDL_CreateTexture(button->widget.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, button->widget.rect.w, button->widget.rect.h);
    button->pressed_image = SDL_CreateTexture(button->widget.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, button->widget.rect.w, button->widget.rect.h);
    button->hover_image = SDL_CreateTexture(button->widget.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, button->widget.rect.w, button->widget.rect.h);
    button->active_image = SDL_CreateTexture(button->widget.renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, button->widget.rect.w, button->widget.rect.h);

    ltk_update_button_image(button->theme, button->widget.renderer, button->normal_image, button->widget.rect);
    ltk_update_button_image(button->theme, button->widget.renderer, button->pressed_image, button->widget.rect);
    ltk_update_button_image(button->theme, button->widget.renderer, button->hover_image, button->widget.rect);
    ltk_update_button_image(button->theme, button->widget.renderer, button->active_image, button->widget.rect);
    SDL_SetRenderTarget(button->widget.renderer, NULL);
}

void ltk_draw_button(void *widget)
{
    LtkButton *button = (LtkButton *)widget;
    SDL_RenderCopy(button->widget.renderer, button->normal_image, NULL, &button->widget.rect);
}

LtkButton *ltk_create_button(LtkTheme *theme, SDL_Renderer *renderer, const char *text, void (*callback)(void))
{
    LtkButton *button = malloc(sizeof(LtkButton));

    if (button == NULL)
    {
        printf("Button could not be created.\n");
            exit(1);
    }

    button->widget.renderer = renderer;
    button->widget.update_function = &ltk_update_button_images;
    button->widget.draw_function = &ltk_draw_button;
    button->widget.destroy_function = &ltk_destroy_button;
    button->widget.rect.x = 0;
    button->widget.rect.y = 0;
    /* For testing, will default size of text once text is implemented */
    button->widget.rect.w = 10;
    button->widget.rect.h = 10;

    button->theme = theme->button;

    button->callback = callback;
    button->text = strdup(text);
    button->pressed = 0;
    button->hover = 0;
    button->active = 0;

    button->normal_image = NULL;
    button->pressed_image = NULL;
    button->hover_image = NULL;
    button->active_image = NULL;
    ltk_update_button_images(button);

    return button;
}

void ltk_destroy_button(void *widget)
{
    LtkButton *button = (LtkButton *)widget;
    if (!button)
    {
        printf("Tried to destroy NULL button.\n");
    }
    free(button->text);
    SDL_DestroyTexture(button->normal_image);
    SDL_DestroyTexture(button->pressed_image);
    SDL_DestroyTexture(button->hover_image);
    SDL_DestroyTexture(button->active_image);
    free(button);
}
