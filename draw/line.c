/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "shape.h"

/*
 * Draw a line using Bresenham's line algorithm
 * https://en.wikipedia.org/wiki/Bresenham's_line_algorithm
 * Code taken from https://rosettacode.org/wiki/Bitmap/Bresenham's_line_algorithm#C
 */
void LTK_DrawLineAliased(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a)
{
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
        int deltax = abs(x1 - x0);
        int sx = x0 < x1 ? 1 : -1;
        int deltay = abs(y1 - y0);
        int sy = y0 < y1 ? 1 : -1; 
        int error = (deltax > deltay ? deltax : -deltay) / 2;
        int error2;
        while (x0 != x1 || y0 != y1) {
                SDL_RenderDrawPoint(renderer, x0, y0);
                error2 = error;
                if (error2 > -deltax) {
                        error -= deltay;
                        x0 += sx;
                }
                if (error2 < deltay) {
                        error += deltax;
                        y0 += sy;
                }
        }
}
/*
void LTK_DrawLineAliased(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a)
{
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
        int x, y;
        double slope = abs((double)(y1 - y0) / (double)(x1 - x0));
        printf("%d, %d; %d, %d; %d, %d; %f\n", x0, y0, x1, y1, x1 - x0, y1 - y0, slope);
        if (slope <= 0.5) {
                if (x0 > x1) {
                        int temp = x0;
                        x0 = x1;
                        x1 = temp;

                        temp = y0;
                        y0 = y1;
                        y1 = temp;
                }

                for (int x = x0; x < x1; x++) {
                        y = slope * (double)(x - x0) + (double)y0;
                        SDL_RenderDrawPoint(renderer, x, y);
                }
        } else {
                double slope_inv = abs((double)(x1 - x0) / (double)(y1 - y0));
                if (y0 > x1) {
                        int temp = y0;
                        y0 = y1;
                        y1 = temp;

                        temp = x0;
                        x0 = x1;
                        x1 = temp;
                }

                for (int y = y0; y < y1; y++) {
                        x = slope_inv * (double)(y - y0) + (double)x0;
                        SDL_RenderDrawPoint(renderer, x, y);
                }
        }
}
*/

static int ipart(double x)
{
        return (int)(x);
}

static double fpart(double x)
{
        if (x < 0) {
                return 1 - (x - floor(x));
        }
        return x - floor(x);
}

static double rfpart(double x)
{
        return 1 - fpart(x);
}

/*
 * Draw an antialiased line using Xiaolin Wu's line algorithm
 * https://en.wikipedia.org/wiki/Xiaolin_Wu's_line_algorithm
 */
void LTK_DrawLineAntialiased(SDL_Renderer *renderer, int x0, int y0, int x1, int y1, int r, int g, int b, int a)
{
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
        int steep = abs(y1 - y0) > abs(x1 - x0);

        if (steep) {
                int temp;
                temp = y0;
                y0 = x0;
                x0 = temp;

                temp = y1;
                y1 = x1;
                x1 = temp;
        }

        if (x0 > x1) {
                int temp;
                temp = x0;
                x0 = x1;
                x1 = temp;

                temp = y0;
                y0 = y1;
                y1 = temp;
        }

        int dx = x1 - x0;
        int dy = y1 - y0;

        double gradient = (double)dy / (double)dx;

        int xend = round(x0);
        int yend = y0 + gradient * (double)(xend - x0);
        double xgap = rfpart(x0 + 0.5);
        int xpxl1 = xend;
        int ypxl1 = ipart(yend);

        if (steep) {
                SDL_SetRenderDrawColor(renderer, r, g, b, (rfpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl1, ypxl1);

                SDL_SetRenderDrawColor(renderer, r, g, b, (fpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, ypxl1 + 1, xpxl1);
        } else {
                SDL_SetRenderDrawColor(renderer, r, g, b, (rfpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl1, ypxl1);

                SDL_SetRenderDrawColor(renderer, r, g, b, (fpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl1, ypxl1 + 1);
        }

        double intery = yend + gradient;

        xend = round(x1);
        yend = y1 + gradient * (double)(xend - x1);
        xgap = rfpart(x1 + 0.5);
        int xpxl2 = xend;
        int ypxl2 = ipart(yend);
        if (steep) {
                SDL_SetRenderDrawColor(renderer, r, g, b, (rfpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl2, ypxl2);

                SDL_SetRenderDrawColor(renderer, r, g, b, (fpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, ypxl2 + 1, xpxl2);
        } else {
                SDL_SetRenderDrawColor(renderer, r, g, b, (rfpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl2, ypxl2);

                SDL_SetRenderDrawColor(renderer, r, g, b, (fpart(yend) * xgap) * a);
                SDL_RenderDrawPoint(renderer, xpxl2, ypxl2 + 1);
        }

        for (int x = xpxl1 + 1; x <= xpxl2 - 1; x++) {
                if (steep) {
                        SDL_SetRenderDrawColor(renderer, r, g, b, (int)(rfpart(intery) * a));
                        SDL_RenderDrawPoint(renderer, ipart(intery), x);

                        SDL_SetRenderDrawColor(renderer, r, g, b, (int)(fpart(intery) * a));
                        SDL_RenderDrawPoint(renderer, ipart(intery) + 1, x);
                } else {
                        SDL_SetRenderDrawColor(renderer, r, g, b, (int)(rfpart(intery) * a));
                        SDL_RenderDrawPoint(renderer, x, ipart(intery));

                        SDL_SetRenderDrawColor(renderer, r, g, b, (int)(fpart(intery) * a));
                        SDL_RenderDrawPoint(renderer, x, ipart(intery) + 1);
                }

                intery += gradient;
        }
}
