/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ltk.h"

/*
 * Do basic SDL initialization stuff.
 */
void ltk_init_sdl(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        exit(1);
    }
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
    {
        printf("Warning: Linear texture filtering not enabled!\n");
    }
}

/*
 * Initialize LTK.
 */
void ltk_init(void)
{
    ltk_init_sdl();
    ltk_window_init();
}

/*
 * Free up any remaining LTK windows and quit SDL.
 */
void ltk_quit(void)
{
    LtkWindow *s;
    /* Loop over all remaining windows and destroy them. */
    for (s = ltk_window_hash; s != NULL; s = s->hh.next)
    {
        if (s)
        {
            ltk_destroy_window(s);
        }
    }
    SDL_Quit();
    exit(0);
}

void ltk_draw(void)
{
    LtkWindow *s;
    for (s = ltk_window_hash; s != NULL; s = s->hh.next)
    {
        if (s)
        {
            ltk_draw_window(s);
        }
    }
}

void ltk_mainloop(void)
{
    SDL_StartTextInput();
    SDL_Event event;
    int fps = 60;
    int ticks_per_frame = 1000 / fps;
    int start_time;
    int end_time;
    int delay_time;
    while (1)
    {
        start_time = SDL_GetTicks();
        if (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                ltk_quit();
                break;
            default:
                ltk_handle_event(event);
            }
        }
        ltk_draw();
        end_time = SDL_GetTicks();
        delay_time = ticks_per_frame - (end_time - start_time);
        if (delay_time > 0)
        {
            SDL_Delay(delay_time);
        }
    }
}
