/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016 Lumidify Productions <lumidify@openmailbox.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LTK_GRADIENT_H_
#define _LTK_GRADIENT_H_

int LTK_DrawVerticalGradient(SDL_Renderer *renderer, int x, int y, int width, int height, int r1, int g1, int b1, int a1, int r2, int g2, int b2, int a2);

int LTK_DrawHorizontalGradient(SDL_Renderer *renderer, int x, int y, int width, int height, int r1, int g1, int b1, int a1, int r2, int g2, int b2, int a2);

#endif
